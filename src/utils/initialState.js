export const initialStateItems = {
    items: [],
    itemSelected: [],
};

export const initialStateSuppliers = {
    suppliers: []
};

export const initialStateUtils = {
    nextCodeItem: "",
    nextCodePriceReduction: "",
};

export const initialStateUsers = {
    users: [],
    userSelected: [],
    userLogged: []
};

export const initialStateRoles= {
    roles: []
};
