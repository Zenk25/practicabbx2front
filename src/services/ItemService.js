import axios from "axios";
import { loadItems, addNewItem, updateItem, loadItemByItemCode, deleteItem } from "../actions/ItemAction";

import { basicUrl } from '../constant/BasicUrl';




export const getItems = async (dispatch) => {


    const token = localStorage.getItem('token');

    await axios.get(`${basicUrl}/items/findItems`, { 'headers': { 'Authorization': ` ${token}` } })
        .then((response => {
            dispatch(loadItems(response.data));
        }));
}

export const getItem = async (dispatch, itemCode) => {

    const token = localStorage.getItem('token');

    await axios.get(`${basicUrl}/items/item/${itemCode}`, { 'headers': { 'Authorization': ` ${token}` } })
        .then((response => {
            dispatch(loadItemByItemCode(response.data));
        }));
}

export const addItem = async (dispatch, item) => {

    const token = localStorage.getItem('token');
    item.creator = localStorage.getItem('user');
    if (item && item.stateEnum === "") {
        item.stateEnum = "ACTIVE";
    }
    await axios.post(`${basicUrl}/items/item/save`, item, { 'headers': { 'Authorization': ` ${token}` } })
        .then((response => {
            dispatch(addNewItem(response.data));
        }));
}

export const updateItemServ = async (dispatch, item, itemCode) => {

    const token = localStorage.getItem('token');

    await axios.put(`${basicUrl}/items/item/update/${itemCode}`, item, { 'headers': { 'Authorization': ` ${token}` } })
        .then((response => {
            dispatch(updateItem(response.data));
        }));
}

export const updateItemServState = async (dispatch, item, itemCode) => {


    const token = localStorage.getItem('token');

    await axios.put(`${basicUrl}/items/item/updateState/${itemCode}`, item, { 'headers': { 'Authorization': ` ${token}` } })
        .then((response => {
            dispatch(updateItem(response.data));
        }));
}

export const deleteItems = async (dispatch, itemCode) => {

    const token = localStorage.getItem('token');

    await axios.delete(`${basicUrl}/items/item/delete/${itemCode}`, { 'headers': { 'Authorization': ` ${token}` } })
        .then((response => {
            dispatch(deleteItem(response.data));
        }));
}
