import axios from "axios";
import { loadRoles } from "../actions/RoleAction";
import { basicUrl } from "../constant/BasicUrl";


export const getRoles = async (dispatch) => {
    
const token = localStorage.getItem('token');

    await axios.get(`${basicUrl}/role/findAll`, { 'headers': { 'Authorization': ` ${token}` } })
        .then((response => {
            dispatch(loadRoles(response.data));
        }));


}

