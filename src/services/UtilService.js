import axios from "axios";
import { getNextCodeItem, getNextCodePriceReduction } from "../actions/UtilAction";
import { basicUrl } from "../constant/BasicUrl";




export const getNextCode = async (dispatch, object) => {
    
const token = localStorage.getItem('token');

    await axios.get(`${basicUrl}/util/nextCode/${object}` , { 'headers': { 'Authorization': `${token}` } })
        .then((response => {
            if (object === 'item') {
                dispatch(getNextCodeItem(response.data));
            } else {
                dispatch(getNextCodePriceReduction(response.data));
            }
        }));

    
}

export const getNextCodeGenerator = async (dispatch, code) => {
    
const token = localStorage.getItem('token');

    await axios.get(`${basicUrl}/util/nextCodeGenerator/${code}` , { 'headers': { 'Authorization': `${token}` } })
        .then((response => {
            dispatch(getNextCodePriceReduction(response.data));
        }));
}
