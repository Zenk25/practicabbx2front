import axios from "axios";
import { addNewUser, deleteUser, loadUser, loadUsers } from "../actions/UserAction";

import { basicUrl } from '../constant/BasicUrl';



export const getUsers = async (dispatch) => {
    
const token = localStorage.getItem('token');

    await axios.get(`${basicUrl}/user/findUsers`, { 'headers': { 'Authorization': `${token}` } })
        .then((response => {
            dispatch(loadUsers(response.data));
        }));
}

export const getUserByUsername = async (dispatch, username) => {
    
const token = localStorage.getItem('token');

    await axios.get(`${basicUrl}/user/get/${username}`,{ 'headers': { 'Authorization': `${token}` } })
        .then((response => {
            dispatch(loadUser(response.data));
        }));
}

export const addUser = async (dispatch, user) => {
    
const token = localStorage.getItem('token');
    
    await axios.post(`${basicUrl}/user/save`, user, {
        'headers':{ 'Authorization': `${token}` }
    })
        .then((response => {
            dispatch(addNewUser(response.data));
        }));
}

export const deleteUserServ = async (dispatch,userName) => {
    
const token = localStorage.getItem('token');

    await axios.delete(`${basicUrl}/user/delete/${userName}`,{ 'headers': { 'Authorization': `${token}` } })
        .then((response => {
            dispatch(deleteUser(response.data));
        }));
}
