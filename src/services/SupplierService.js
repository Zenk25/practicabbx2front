import axios from "axios";
import { loadSuppliers } from "../actions/SupplierAction";
import { basicUrl } from "../constant/BasicUrl";



export const getSuppliers = async (dispatch) => {
    
const token = localStorage.getItem('token');
    
    await axios.get(`${basicUrl}/suppliers/findSuppliers`, { 'headers': { 'Authorization': `${token}` } })
        .then((response => {
            dispatch(loadSuppliers(response.data));
        }));
}