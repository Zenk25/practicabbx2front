import React from "react";
import { Switch, Redirect } from "react-router-dom";
import { Navbar } from "../components/ui/NavBar";
import { ItemDetails } from "../pages/ItemDetails";
import { ItemScreen } from "../pages/ItemScreen";
import { UserScreen } from "../pages/UserScreen";
import { PrivateRoute } from "./PrivateRoute";



export const DashboardRoutes = () => {

  function checkRole() {
    const currentRoles = localStorage.getItem('role');
    return currentRoles && currentRoles === "\"ROLE_ADMIN\"";
  }

  function isLogged() {
    const logged = localStorage.getItem('role');
    if (logged) {
      return true;
    } else {
      return false;
    }
  }



  return (
    <>
      <Navbar />
      <div className="container mt-2" style={{ margin: '0px 40px 10px 40px' }}>
        <Switch>
          <PrivateRoute
            path="/item"
            component={ItemScreen}
            isAuthenticated={isLogged()}
          />
          <PrivateRoute
            path="/user"
            component={UserScreen}
            isAuthenticated={checkRole()}
          />
          <PrivateRoute
            exact path="/itemDetails/:itemCode"
            component={ItemDetails}
            isAuthenticated={isLogged()}
          />

          <Redirect to="/login" />
        </Switch>
      </div>

      {/* <Footer /> */}
    </>
  );
};
