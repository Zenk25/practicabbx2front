import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';
import { authReducer } from "../reducers/authReducer";
import { itemReducer } from "../reducers/ItemReducer";
import { roleReducer } from "../reducers/RoleReducer";
import { supplierReducer } from "../reducers/SupplierReducer";
import { userReducer } from "../reducers/UserReducer";
import { utilReducer } from "../reducers/UtilReducer";


const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
  auth: authReducer,
  items:itemReducer,
  suppliers: supplierReducer,
  nextCode: utilReducer,
  users: userReducer,
  roles: roleReducer
  
})

export const store = createStore(
  reducers,
  composeEnhancers(
      applyMiddleware( thunk )
  )
);

