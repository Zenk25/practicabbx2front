import { UserComponent } from "../components/user/UserComponent";

export const UserScreen = ({ history }) => {

  return (
    <>
      <br />
      <h3>User List</h3>
      <hr />
      <UserComponent history={history} />
    </>
  );
};