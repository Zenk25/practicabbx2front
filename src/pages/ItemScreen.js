import React from "react";
import { ItemComponent } from "../components/Item/ItemComponent";

export const ItemScreen = ({ history }) => {

  return (
    <>
      <br />
      <h3>Item List</h3>
      <hr />
      <ItemComponent history={history} />
    </>
  );
};