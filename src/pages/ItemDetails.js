import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom';
import { getItem } from '../services/ItemService';




export const ItemDetails = () => {
    const params = useParams();

    const dispatch = useDispatch();

    const items = useSelector(state => state.items.items);

    const item = items.find(e => e.itemCode === params.itemCode);

    const history = useHistory();

    function goBack() {
        history.replace("/item");
    }

    function stateControl(value) {
        if (value === "DISCONTINUED") {
            return <p><b>Reason: </b><p>{item && item.messageDisabled}</p></p>
        }
    }

    useEffect(() => {
       getItem(dispatch, params.itemCode);
    }, [dispatch, params.itemCode]);

    return (
        <>
            <br />
            <br />
            <br />
            <div className="card">
                <div className="card-header">
                    <h2>{item && item.description}</h2>
                </div>
                <div className="card-body">
                    <h5 className="card-title">CODE: {item && item.itemCode}</h5>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                            <p><b>Creation Date: </b>{item && item.creationDate}</p>
                        </li>
                        <li className="list-group-item">
                            <p><b>State: </b><p>{item && item.stateEnum}</p></p>
                            {stateControl(item.stateEnum)}

                        </li>
                        
                        <li className="list-group-item">
                            <div><b>Suppliers: </b>{
                                item.suppliers && item.suppliers.map((supplier, index) => {
                                    return (

                                        <div className="row">
                                            <ol></ol>
                                            <div className="col-sm">
                                                - Name: {supplier.name}
                                            </div>
                                            <div className="col-sm">
                                                - Conuntry: {supplier.country}
                                            </div>
                                        </div>
                                    );
                                })
                            }</div>
                        </li>
                        <li className="list-group-item">
                            <div><b>Price Reduction: </b>{
                                item.priceReductions && item.priceReductions.map((priceReduct, index) => {
                                    return (

                                        <div className="row">
                                            <ol></ol>
                                            <div className="col-sm">
                                                - Reduction: {priceReduct.reducedPrice} €
                                        </div>
                                            <div className="col-sm">
                                                - Start Date: {priceReduct.startDate}
                                            </div>
                                            <div className="col-sm">
                                                - End Date: {priceReduct.endDate}
                                            </div>
                                        </div>
                                    );
                                })
                            }</div>
                        </li>
                        <li className="list-group-item">
                            <p><b>Price: </b>{item.price}</p>
                        </li>
                    </ul>
                    <button className="btn btn-primary" onClick={goBack}>Go Back</button>
                </div>
            </div>
        </>
    )
}
