import { LOAD_ROLES } from "../types/types";

export const loadRoles = (data) => ({type: LOAD_ROLES , data});
