
import { LOGIN } from '../types/types';

import axios from "axios";
import { basicUrl } from '../constant/BasicUrl';
import swal from 'sweetalert';




export const startLogin = async (email, pass, dispatch) => {

    await axios.post(`${basicUrl}/login`, {}, {
        auth:
        {
            username: email,
            password: pass
        }
    })
        .then((response => {
            localStorage.clear();
            localStorage.setItem('token', "Bearer " + response.headers.authorization);
            login(email, dispatch);
        }), error => {
            swal("Wrong Credentials", "The username or password are incorrect", "error");
        });
}


export const login = async (email, dispatch) => {
    var token = localStorage.getItem('token');
    await axios.get(`${basicUrl}/user/get/${email}`, { 'headers': { 'Authorization': `Bearer ${token}` } })
        .then((response => {
            dispatch(logged(response.data));
        }));

}

export const logged = (user) => {

    localStorage.setItem('idUser', user.idUser);
    localStorage.setItem('user', user.username);
    localStorage.setItem('role', JSON.stringify(user.role.role));
    localStorage.setItem('logged', true);

    return {
        type: LOGIN,
        payload: {
            user
        }
    }
}
