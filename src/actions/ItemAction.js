import { ADD_ITEM, DELETE_ITEM, GET_ITEMS, GET_ITEMS_BY_ITEMCODE, UPDATE_ITEM } from "../types/types";


export const loadItems = (data) => ({type: GET_ITEMS, data});
export const loadItemByItemCode = (data) => ({type: GET_ITEMS_BY_ITEMCODE, data});
export const addNewItem = (data) => ({type: ADD_ITEM, data});
export const updateItem = (data) => ({type: UPDATE_ITEM, data});
export const deleteItem = (data) => ({type: DELETE_ITEM, data});

