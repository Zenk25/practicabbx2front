import { ADD_USER, DELETE_USER, LOAD_USERS, LOAD_USER_BY_USERNAME } from "../types/types";

export const loadUsers = (data) => ({type: LOAD_USERS, data});
export const loadUser = (data) => ({type: LOAD_USER_BY_USERNAME, data})
export const addNewUser = (data) => ({type: ADD_USER, data});
export const deleteUser = (data) => ({type: DELETE_USER, data});