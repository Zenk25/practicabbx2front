import { LOAD_SUPPLIER } from "../types/types";

export const loadSuppliers = (data) => ({type: LOAD_SUPPLIER, data})