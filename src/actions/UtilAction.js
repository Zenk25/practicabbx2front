import { UTIL_CODE_ITEM, UTIL_CODE_PRICEREDUCTION } from "../types/types";

export const getNextCodeItem = (data) => ({type: UTIL_CODE_ITEM, data});
export const getNextCodePriceReduction = (data) => ({type: UTIL_CODE_PRICEREDUCTION, data});