import { makeStyles, Switch } from "@material-ui/core";
import clsx from 'clsx';
import { useHistory } from "react-router-dom";
import { Edit, Delete, Visibility } from "@material-ui/icons";

export const useStyles = makeStyles({
  root: {
    '& .tableCell.active': {
      color: 'green'
    },
    '& .tableCell.discontinued': {
      color: 'red'
    },
  },
  iconos: {
    cursor: "pointer",
  }
});

export const itemHeadCells = () => {
  return ItemDataColumns.map(e => {
    return { headerName: e.title, field: e.field }
  })
}


export const ItemDataColumns = (handleOpenCloseEdit, handleOpenCloseDelete, handleOpenCloseDisabled, handleActivate) => {

  const styles = useStyles();
  const history = useHistory();

  return (
    [
      { headerName: 'Item Code', field: 'itemCode', flex: 0.5 },
      { headerName: 'Description', field: 'description', flex: 1 },
      { headerName: 'Creation Date', field: 'creationDate', flex: 0.30 },
      { headerName: 'Price', field: 'price' },
      {
        headerName: 'State', field: 'stateEnum', cellClassName: (params) => clsx('tableCell', {
          active: params.row.stateEnum === 'ACTIVE',
          discontinued: params.row.stateEnum === 'DISCONTINUED'
        }), renderCell: (params) => {
          
          const handleSwitch = () => {
            if(params.row.stateEnum === 'ACTIVE'){
              return true;
            }else{
              return false;
            }
          };

          const handleChangeOpen = () => {
            if(handleSwitch()){
              handleOpenCloseDisabled(params.row.itemCode);
            }else{
              handleActivate(params.row.itemCode);
            }
          }

          return (
            <strong>{params.row.stateEnum}
              <Switch color="primary" checked={handleSwitch()} onClick={() => handleChangeOpen()} name="checkedC" />
            </strong>
            )
          }, flex: 0.5
      },
      /*{
        headerName: 'Suppliers', field: 'suppliers', renderCell: (params) => {
          return (
            `${params.row.suppliers.map((supplier) => {
              return ("\nName: " + supplier.name)
            })}`
          )
        }, flex: 1.5
      },
      {
        headerName: 'Price Reductions', field: 'priceReductions', renderCell: (params) => {
          return (
            `${params.row.priceReductions.map((priceReduction) => {
              return ("\n-" + priceReduction.reducedPrice)
            })}`
          )
        }, flex: 1
      },*/
      { headerName: 'Creator', field: 'creator' },
      {
        headerName: 'Actions', field: '', renderCell: (params) => {

          

          const toItemDetails = () => {
            const api = params.api;
            const fields = api
              .getAllColumns()
              .map((c) => c.field)
              .filter((c) => c !== "__check__" && !!c);
            const thisRow = {};

            fields.forEach((f) => {
              thisRow[f] = params.getValue(f);
            });

            history.push(`/itemDetails/${thisRow.itemCode}`);
          };
          const editItem = () => {
            const api = params.api;
            const fields = api
              .getAllColumns()
              .map((c) => c.field)
              .filter((c) => c !== "__check__" && !!c);
            const thisRow = {};

            fields.forEach((f) => {
              thisRow[f] = params.getValue(f);
            });
            return thisRow;
          };

          const handleActive = () => {
            if(editItem().stateEnum === 'ACTIVE'){
              return true;
            }else{
              return false;
            }
          }

          const checkRole = () => {
            const role = localStorage.getItem('role');
            return role && role === "\"ROLE_ADMIN\"";
          }
          
          return <div>
            <Visibility className={styles.iconos} style={{padding: 2, minWidth: 12}} onClick={toItemDetails} color="primary"></Visibility>
            {handleActive() ? 
            (<Edit className={styles.iconos} style={{padding: 2, minWidth: 12}} onClick={() => handleOpenCloseEdit(editItem().itemCode)} color="action"></Edit>) 
            : (<Edit className={styles.iconos} style={{padding: 2, minWidth: 12}} color="disabled"></Edit>) }
             {checkRole() ? 
            (<Delete className={styles.iconos} style={{padding: 2, minWidth: 12}} onClick={() => handleOpenCloseDelete(editItem().itemCode)} color="secondary"></Delete>) 
            : (<Delete className={styles.iconos} style={{padding: 2, minWidth: 12}} color="disabled"></Delete>) }
            
          </div>

        }, flex: 0.30
      }]
  )
};
