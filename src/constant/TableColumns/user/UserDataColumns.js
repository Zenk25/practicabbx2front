import { makeStyles } from "@material-ui/core";
import { Delete } from "@material-ui/icons";



export const useStyles = makeStyles({
  root: {
    color: 'green'
    
  },
  rootno: {
    color: 'red'

  },
  iconos: {
    cursor: "pointer",
  }
});




export const UserDataColumns = (handleOpenCloseDelete) => {

  const styles = useStyles();

  return (
    [
      { headerName: 'Username', field: 'username', flex: 0.5 },
      {
        headerName: 'Role', field: 'role.role', 
        renderCell: (params) => (
          params.row.role.role === 'ROLE_ADMIN' ? (<strong className={styles.root}>{params.row.role.description}</strong>)
          : 
            (<strong className={styles.rootno}>{params.row.role.description}</strong>)
          
          ), flex: 0.75
      },
      {
        headerName: 'Actions', field: '', renderCell: (params) => {
          const userRow = () => {
            const api = params.api;
            const fields = api
              .getAllColumns()
              .map((c) => c.field)
              .filter((c) => c !== "__check__" && !!c);
            const thisRow = {};

            fields.forEach((f) => {
              thisRow[f] = params.getValue(f);
            });
            return thisRow;
          };
          return <div>
            <Delete className={styles.iconos} style={{padding: 2, minWidth: 12}} onClick={() => handleOpenCloseDelete(userRow().username)}></Delete>
          </div>

        }, flex: 0.5
      }]
  )
};
