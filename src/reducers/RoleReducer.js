import { LOAD_ROLES } from "../types/types"
import { initialStateRoles } from "../utils/initialState"

export const roleReducer = (state = initialStateRoles, action) => {
    switch (action.type) {
        case LOAD_ROLES:
            return {
                ...state,
                roles: action.data
            }
       
        default:
            return state
    }
}