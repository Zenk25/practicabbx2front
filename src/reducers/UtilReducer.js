import { UTIL_CODE_ITEM, UTIL_CODE_PRICEREDUCTION } from "../types/types";
import { initialStateUtils } from "../utils/initialState";



export const utilReducer = (state = initialStateUtils, action) => {
    switch (action.type) {
        case UTIL_CODE_ITEM:
            return {
                ...state,
                nextCodeItem: action.data
            }
        case UTIL_CODE_PRICEREDUCTION:
            return {
                ...state,
                nextCodePriceReduction: action.data
            }
        default:
            return state
    }
}