import { ADD_USER, DELETE_USER, LOAD_USERS, LOAD_USER_BY_USERNAME } from "../types/types";
import { initialStateUsers } from "../utils/initialState";



export const userReducer = (state = initialStateUsers, action) => {
    switch (action.type) {
        case LOAD_USERS:
            return {
                ...state,
                users: action.data
            }
        case LOAD_USER_BY_USERNAME:
                return {
                    ...state,
                    userSelected: action.data
                }
        case ADD_USER:
            return {
                ...state,
                users: [...state.users, action.data]
            }
        case DELETE_USER:
            return {
                ...state,
                users: state.users.filter(
                    user => user.username !== action.data
                )
            }
        default:
            return state
    }
}