import { LOAD_SUPPLIER } from "../types/types";
import { initialStateSuppliers } from "../utils/initialState";


export const supplierReducer = (state = initialStateSuppliers, action) => {

    switch(action.type){
        case LOAD_SUPPLIER:
            return{
                ...state,
                suppliers :action.data
            }
        default: 
            return state;
    }
}