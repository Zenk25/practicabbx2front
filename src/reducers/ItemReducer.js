import { ADD_ITEM, DELETE_ITEM, GET_ITEMS, GET_ITEMS_BY_ITEMCODE, UPDATE_ITEM } from "../types/types";
import { initialStateItems }  from "../utils/initialState";



export const itemReducer = (state = initialStateItems, action) => {
    switch(action.type){
        case GET_ITEMS: 
            return{
                ...state,
                items: action.data
            }
        case GET_ITEMS_BY_ITEMCODE: 
            return{
                ...state,
                itemSelected: action.data
            }
        case ADD_ITEM:
            return {
                ...state,
                items: [...state.items, action.data]
            }
            
        case UPDATE_ITEM:
            return {
                ...state,
                items: state.items.map(
                    item => item.itemCode === action.data.itemCode
                        ? action.data : item
                )
            }
        case DELETE_ITEM:
            return {
                ...state,
                items : state.items.filter(
                    item => item.itemCode !== action.data
                )
            }
        default:
            return state
    }
}