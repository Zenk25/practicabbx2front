//Auth
export const LOGIN = 'Login'; 
export const LOGOUT = 'Logout'; 

//Items
export const GET_ITEMS = 'Get Items';
export const GET_ITEMS_BY_ITEMCODE = 'Get Item by itemCode';
export const ADD_ITEM = 'Add Item';
export const UPDATE_ITEM = 'Update Item';
export const DELETE_ITEM = 'Delete Item';

//Util
export const UTIL_CODE_ITEM = 'Get next Code Item';
export const UTIL_CODE_PRICEREDUCTION = 'Get next Code PriceReduction';

//Supplier
export const LOAD_SUPPLIER = 'Get All Suppliers';

//User
export const LOAD_USERS = 'Get Users';
export const LOAD_USER_BY_USERNAME = 'Get User by username';
export const ADD_USER = 'Add User';
export const DELETE_USER = 'Delete User';

//Roles
export const LOAD_ROLES = 'Get Roles';