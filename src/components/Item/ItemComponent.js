import { Fab, Grid, Modal } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addItem, deleteItems, getItem, getItems, updateItemServ, updateItemServState } from '../../services/ItemService';
import AddIcon from "@material-ui/icons/Add";
import { ModalSave } from './modals/ModalSave';
import { ModalDelete } from './modals/ModalDelete';
import { getSuppliers } from '../../services/SupplierService';
import { ItemTable } from './Tables/ItemTable';
import { ItemDataColumns, useStyles } from '../../constant/TableColumns/Item/ItemDataColumns';
import Switch from '@material-ui/core/Switch';
import { getNextCode, getNextCodeGenerator } from '../../services/UtilService';
import { ModalDisabled } from './modals/ModalDisabled';



export function ItemComponent({ history }) {

    const dispatch = useDispatch();

    const [edit, setEdit] = useState(false);
    const [modalSave, setModalSave] = useState(false);
    const [modalDisabled, setModalDisabled] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [flagState, setFlagState] = useState(false);
    const [stateFilter, setStateFilter] = useState(true);


    const classesTable = useStyles();

    


    const [stateFilterModel, setStateFilterModel] = useState({
        items:
            [{ columnField: 'stateEnum', operatorValue: 'contains', value: 'ACTIVE' },
            ]
    });

    useEffect(() => {
        getItems(dispatch);
        getSuppliers(dispatch);
        getNextCode(dispatch, 'item');
        getNextCode(dispatch, 'priceReduction');
    }, [dispatch, flagState]);


    

    const handleNextCodeGenerator = (code) => {
        getNextCodeGenerator(dispatch, code);

    }

    //CONST MODALS
    const modalOpenCloseSave = () => {
        setModalSave(!modalSave);
    }

    const modalOpenCloseDisabled = () => {
        setModalDisabled(!modalDisabled);
    }

    const modalOpenCloseDelete = () => {
        setModalDelete(!modalDelete);
    }



    const handleStateChange = () => {
        if (stateFilter) {
            setStateFilterModel({ items: [{ columnField: 'stateEnum', operatorValue: 'contains', value: 'DISCONTINUED' }] });
            setStateFilter(false);
        } else {
            setStateFilterModel({ items: [{ columnField: 'stateEnum', operatorValue: 'contains', value: 'ACTIVE' }] });
            setStateFilter(true);
        }
    }

    //HANDLES MODALS OPENCLOSE
    const handleOpenCloseEdit = (itemCode) => {
        getItem(dispatch, itemCode);
        setEdit(true);
        modalOpenCloseSave();
    }

    const handleOpenCloseDisabled = (itemCode) => {
        getItem(dispatch, itemCode);
        modalOpenCloseDisabled();
    }

    const handleOpenCloseDelete = (itemCode) => {
        getItem(dispatch, itemCode);
        modalOpenCloseDelete();
    }

    const handleOpenCloseSave = () => {
        setEdit(false);
        modalOpenCloseSave();
    }

    //HANDLES SAVES,UPDATE, DELETE
    const handleSave = (newItem) => {
        addItem(dispatch, newItem);
        setFlagState(true);
        getNextCode(dispatch, 'item');
        modalOpenCloseSave();
    };

    const handleUpdate = (item) => {
        updateItemServ(dispatch, item, item.itemCode);
        setFlagState(true);
        modalOpenCloseSave();
    }

    const handleUpdateState = (item) => {
        updateItemServState(dispatch, item, item.itemCode);
        setFlagState(true);
        modalOpenCloseDisabled();
    }

    const itemState = useSelector((state) => state.items.itemSelected);

    const handleActivate = (itemCode) => {
        getItem(dispatch,itemCode)
        updateItemServState(dispatch, itemState, itemState.itemCode);
        setFlagState(true);
    }

    const handleDeleteItem = (itemCode) => {
        deleteItems(dispatch, itemCode);
        setFlagState(true);
        modalOpenCloseDelete();
    }

    const items = useSelector((state) => state.items.items);
    const suppliers = useSelector((state) => state.suppliers.suppliers);
    const nextCode = useSelector(state => state.nextCode);


    const createData = (id, itemCode, description, creationDate, price, stateEnum, suppliers, priceReductions, creator, user) => {
        return { id, itemCode, description, creationDate, price, stateEnum, suppliers, priceReductions, creator, user }
    }

    const data = items.map((item, id) => {
        return (
            createData(id, item.itemCode, item.description, item.creationDate, item.price, item.stateEnum, item.suppliers, item.priceReductions, item.creator, 'user')
        )
    }
    )


    if (items !== null && items !== undefined) {

        return (
            <>
                <div style={{ display: 'vertical-flex' }}>
                    <div style={{ marginBottom: 12 }} className="text-center">
                        <br />
                        <div style={{ height: 600, width: '100%' }} className={classesTable.root}>
                            <Grid xs={12} container direction="row" justify="flex-end" alignItems="center" style={{marginBottom:10}}>
                                <Grid xs={3} >
                                    <Grid justify="flex-end" component="label" container alignItems="center" spacing={1} >
                                        <Grid item style={{ 'font-size': 'smaller' }}><strong>DISCONTINUED</strong></Grid>
                                            <Switch color="primary" checked={stateFilter} onChange={handleStateChange} name="checkedC" />
                                        <Grid item style={{ 'font-size': 'smaller' }}><strong>ACTIVE</strong></Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <ItemTable
                                columns={() => ItemDataColumns(handleOpenCloseEdit, handleOpenCloseDelete, handleOpenCloseDisabled, handleActivate)}
                                data={data}
                                filterModel={stateFilterModel}
                            />
                        </div>
                    </div>
                    <Modal open={modalSave} onClose={modalOpenCloseSave}>
                        {edit ? (
                            <ModalSave handleSave={handleSave} modalOpenCloseSave={modalOpenCloseSave} suppliers={suppliers} nextCode={nextCode} handleNextCodeGenerator={handleNextCodeGenerator} edit={edit} handleUpdate={handleUpdate}></ModalSave>
                        ) : (
                                <ModalSave handleSave={handleSave} modalOpenCloseSave={modalOpenCloseSave} suppliers={suppliers} nextCode={nextCode} handleNextCodeGenerator={handleNextCodeGenerator}></ModalSave>
                            )}
                    </Modal>
                    <Modal open={modalDelete} onClose={modalOpenCloseDelete}>
                        <ModalDelete handleDeleteItem={handleDeleteItem} modalOpenCloseDelete={modalOpenCloseDelete} />
                    </Modal>
                    <Modal open={modalDisabled} onClose={modalOpenCloseDisabled}>
                        <ModalDisabled modalOpenCloseDisabled={modalOpenCloseDisabled}  edit={edit} handleUpdate={handleUpdateState}></ModalDisabled>
                    </Modal>
                    <div style={{ marginTop: 50 }}>
                        <Fab style={{ margin: 10 }} onClick={() => handleOpenCloseSave()} color="primary" aria-label="add">
                            <AddIcon />
                        </Fab>
                        {/*Boton para cambiar el filtro del Estado
                    <Fab style={{margin: 10}} onClick={() => handleStateChange()} color="secondary" aria-label="favorite">
                        <FavoriteIcon />
                    </Fab>
                    */}
                    </div>
                </div>
            </>
        );
    }
}