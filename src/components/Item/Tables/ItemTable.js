import { DataGrid } from "@material-ui/data-grid";




export const ItemTable = (props) => {

    
    return (
        <DataGrid 
            rows={props.data}
            columns={props.columns()} 
            rowHeight={100} 
            pageSize={5} 
            rowsPerPageOptions={[5, 10, 20]} 
            filterModel={props.filterModel}
             />
            

    );
}