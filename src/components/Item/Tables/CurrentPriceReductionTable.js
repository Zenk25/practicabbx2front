import { DataGrid } from "@material-ui/data-grid";




export const CurrentPriceReductionTable = (props) => {

    
    return (
        <DataGrid 
            rows={props.data}
            columns={props.columns} 
            rowHeight={100} 
            pageSize={5} 
            rowsPerPageOptions={[5, 10, 20]} 
            /*Meter filtro para saber Que priceReduction esta activo
            filterModel={props.filterModel}*/
             />
            

    );
}