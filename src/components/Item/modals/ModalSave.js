import { Button, Fab, Grid, InputLabel, makeStyles, Modal, NativeSelect, Select, TextField } from '@material-ui/core';
import AddIcon from "@material-ui/icons/Add";
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { ModalNewPriceReduction } from './ModalNewPriceReduction';




export const ModalSave = React.forwardRef((props, ref) => {



    const itemSelected = useSelector((state) => state.items.itemSelected);
    const nextCode = props.nextCode;
    const [modalPriceReduction, setModalPriceReduction] = useState(false);


    const [newItem, setNewItem] = useState({
        itemCode: nextCode.nextCodeItem,
        description: "",
        creationDate: "",
        price: "",
        priceReductions: [],
        suppliers: [],
        stateEnum: "",
        creator: "user"

    });

    useEffect(() => {
        if (props.edit) {
            if (itemSelected) {
                setNewItem(itemSelected);
            }
        }
    }, [props.edit, itemSelected]);




    const handleOnChange = (e) => {
        const { name, value } = e.target;
        setNewItem((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };


    const handleChangeMultiple = (e) => {
        const { options } = e.target;
        const value = [];
        for (let i = 0, l = options.length; i < l; i += 1) {
            if (options[i].selected) {
                value.push(JSON.parse(options[i].value));
            }
        }
        setNewItem((prevState) => ({
            ...prevState,
            suppliers: value
        }));

    };

    const modalOpenClosePriceReduction = () => {
        setModalPriceReduction(!modalPriceReduction);
    };

    const handleSavePriceReduction = (newPriceReduction) => {
        props.handleNextCodeGenerator(newPriceReduction.priceReductionCode);
        setNewItem((prevState) => ({
            ...prevState,
            priceReductions: [...prevState.priceReductions, newPriceReduction]
        }));
    };


    const useStyles = makeStyles((theme) => ({
        modal: {
            position: "absolute",
            width: 400,
            backgroundColor: theme.palette.background.paper,
            border: "2px solid #000",
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
        },

        margin: {
            margin: theme.spacing(1),
        },
        iconos: {
            cursor: "pointer",
        },
        inputMaterial: {
            width: "100%",
            margin: theme.spacing(1)
        },
        inputLabel: {
            width: "100%",
            marginTop: theme.spacing(1),
            textDecoration: "underline",
            color: theme.palette.text.primary

        }
    }));


    const handleSaveOrUpdate = (item) => {
        if (props.edit) {
            props.handleUpdate(item);
        } else {
            props.handleSave(item);
        }
    }

    const styles = useStyles();

    return (
        <form onSubmit={() => handleSaveOrUpdate(newItem)}>
            <div className={styles.modal}>
                <h3>Add new Item</h3>
                <br />
                <TextField
                    name="itemCode"
                    value={newItem.itemCode}
                    required
                    disabled
                    type="text"
                    className={styles.inputMaterial}
                    label="Code"
                    onChange={handleOnChange}
                />
                <br />
                <TextField
                    name="description"
                    required
                    value={newItem.description}
                    className={styles.inputMaterial}
                    label="Description"
                    onChange={handleOnChange}
                />
                <br />
                <TextField
                    name="price"
                    type="number"
                    min="0"
                    value={newItem.price}
                    required
                    className={styles.inputMaterial}
                    label="Price"
                    onChange={handleOnChange}
                    inputProps={{
                        step: "any",
                    }}
                />
                <br />
                <InputLabel className={styles.inputLabel}><strong>SELECT SUPPLIERS</strong></InputLabel>
                <Select
                    multiple
                    native
                    name="suppliers"
                    className={styles.inputMaterial}
                    label="Suppliers"
                    onChange={handleChangeMultiple}
                    inputProps={{
                        id: "select-multiple-native",
                    }}
                >
                    {props.suppliers &&
                        props.suppliers.map((supplier) => {
                            let selected = false;
                            if (newItem.suppliers && newItem.suppliers.some(e => e.supplierCode === supplier.supplierCode)) {
                               selected = true;
                            }else{
                                selected = false;
                            }
                            return(
                            <option
                                    key={supplier.supplierCode}
                                    value={JSON.stringify(supplier)}
                                    selected={selected}
                                >
                                    {supplier.name}
                                </option>
                            )
                        }
                        )}
                </Select>
                <br />

                <InputLabel className="mt-3">State</InputLabel>
                <NativeSelect
                    name="stateEnum"
                    className={styles.inputMaterial}
                    label="State"
                    onChange={handleOnChange}
                    defaultValue="ACTIVE"
                >
                    <option value="ACTIVE">ACTIVE</option>
                    <option value="DISCONTINUED">DISCONTINUED</option>
                </NativeSelect>
                <br />
                <br />
                {/*messageDiscontiuned(itemSelected.stateEnum, 'Save')*/}
                <Grid container spacing={3}>
                    <Grid item xs={9}>
                        <InputLabel className="mt-3">
                            <b>Add Price Reduction</b>
                        </InputLabel>
                    </Grid>
                    <Grid item xs={3}>
                        <Fab
                            size="small"
                            color="secondary"
                            label="Price"
                            aria-label="add"
                            className={styles.margin}
                        >
                            <AddIcon
                                aling="right"
                                onClick={() => modalOpenClosePriceReduction()}
                            />
                        </Fab>
                    </Grid>
                </Grid>
                <br />
                <div align="right">
                    <Button type="submit" color="primary">Save</Button>
                    <Button onClick={props.modalOpenCloseSave}>Cancel</Button>
                </div>
            </div>
            <Modal open={modalPriceReduction} onClose={modalOpenClosePriceReduction}>
                <ModalNewPriceReduction priceReductions={newItem.priceReductions} nextCodePriceReduction={nextCode} modalOpenClosePriceReduction={modalOpenClosePriceReduction} handleSavePriceReduction={handleSavePriceReduction} />
            </Modal>
        </form>
    )
})



