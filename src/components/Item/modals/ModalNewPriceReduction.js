import { Button, InputLabel, makeStyles, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import moment from "moment";
import { useSelector } from 'react-redux';



export const ModalNewPriceReduction = React.forwardRef((props, ref) => {

    
    const useStyles = makeStyles((theme) => ({
        modal: {
            position: "absolute",
            width: 400,
            backgroundColor: theme.palette.background.paper,
            border: "2px solid #000",
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
        },

        margin: {
            margin: theme.spacing(1),
        },
        iconos: {
            cursor: "pointer",
        },
        inputMaterial: {
            width: "100%",
            margin: theme.spacing(1)
        },
        inputLabel: {
            width: "100%",
            marginTop: theme.spacing(1),
            textDecoration: "underline",
            color: theme.palette.text.primary

        }
    }));

    const nextCodePriceReduction = useSelector(state => state.nextCode.nextCodePriceReduction);


    useEffect(() => {
        setNewPriceReduction({
            priceReductionCode:  nextCodePriceReduction,
            reducedPrice: "",
            startDate: "",
            endDate: "",
        });
    }, [nextCodePriceReduction]);


    const priceReductions = props.priceReductions;
    

    const [newPriceReduction, setNewPriceReduction] = useState({
        priceReductionCode: nextCodePriceReduction,
        reducedPrice: "",
        startDate: "",
        endDate: "",
    });

    const handleChangePriceReduction = (e) => {
        const { name, value } = e.target;
        
        setNewPriceReduction((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };


    const handleAddNewPriceReduction = () =>{
        props.handleSavePriceReduction(newPriceReduction);
    }


    



    const styles = useStyles();

    return (

        <div className={styles.modal}>
            <h3>Add Price Reduction</h3>
            <TextField
                required
                type="string"
                disabled
                name="priceReductionCode"
                value={newPriceReduction.priceReductionCode}
                className={styles.inputMaterial}
                label="Price Reduction Code"
                onChange={handleChangePriceReduction}
            />
            <br />
            <TextField
                required
                type="number"
                name="reducedPrice"
                value={newPriceReduction.reducedPrice}
                className={styles.inputMaterial}
                label="Price Reduction"
                onChange={handleChangePriceReduction}
            />
            <br />
            <InputLabel className="mt-3">
                Start Date
            </InputLabel>
            <TextField
                type="date"
                name="startDate"
                value={newPriceReduction.startDate}
                className={styles.inputMaterial}
                onChange={handleChangePriceReduction}
                inputProps={{
                    min: moment().format("YYYY-MM-DD"),
                    onkeydown: false
                }}
            />
            <br />
            <InputLabel className="mt-3">
                End Date
            </InputLabel>
            <TextField
                type="date"
                name="endDate"
                className={styles.inputMaterial}
                value={newPriceReduction.endDate}
                onChange={handleChangePriceReduction}
                inputProps={{
                    min: moment(newPriceReduction.startDate).add(1, 'days').format("YYYY-MM-DD"),
                    onkeydown: false
                }}
            />
            <br />
            <br />
            <hr />
            <TableContainer>
                <InputLabel className="mt-3">Current Price Reduction</InputLabel>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Code</TableCell>
                            <TableCell>Reduction</TableCell>
                            <TableCell>Start Date</TableCell>
                            <TableCell>End Date</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {priceReductions &&
                            priceReductions.map((priceReduc) => (
                                <TableRow>
                                    <TableCell>{priceReduc.priceReductionCode}</TableCell>
                                    <TableCell>{priceReduc.reducedPrice}</TableCell>
                                    <TableCell>{moment(priceReduc.startDate).format("DD/MM/yyyy")}</TableCell>
                                    <TableCell>{moment(priceReduc.endDate).format("DD/MM/yyyy")}</TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <br />
            <br />
            <div align="right">
                <Button onClick={() => handleAddNewPriceReduction(newPriceReduction)} color="primary">
                    Add
            </Button>
                <Button onClick={props.modalOpenClosePriceReduction}>Exit</Button>
            </div>
        </div>

    )
})



