import { Button, makeStyles, TextField } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';




export const ModalDisabled = React.forwardRef((props, ref) => {



    const itemSelected = useSelector((state) => state.items.itemSelected);
    const [modalDisabled, setModalDisabled] = useState(false);


    const [newItem, setNewItem] = useState({
        itemCode: "",
        description: "",
        creationDate: "",
        price: "",
        priceReductions: [],
        suppliers: [],
        stateEnum: "",
        creator: "user"

    });

    useEffect(() => {
        if (itemSelected) {
            setNewItem(itemSelected);
        }
    }, [itemSelected]);



    const handleOnChange = (e) => {
        const { name, value } = e.target;
        setNewItem((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };

    const modalOpenCloseDisabled = () => {
        setModalDisabled(!modalDisabled);
    };

    const handleSaveDeactivate = (itemSelected) => {
        setNewItem((prevState) => ({
            ...prevState,
            messageDisabled:  itemSelected.messageDisabled
        }));
    };


    const useStyles = makeStyles((theme) => ({
        modal: {
            position: "absolute",
            width: 400,
            backgroundColor: theme.palette.background.paper,
            border: "2px solid #000",
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
        },

        margin: {
            margin: theme.spacing(1),
        },
        iconos: {
            cursor: "pointer",
        },
        inputMaterial: {
            width: "100%",
            margin: theme.spacing(1)
        },
        inputLabel: {
            width: "100%",
            marginTop: theme.spacing(1),
            textDecoration: "underline",
            color: theme.palette.text.primary

        }
    }));


    const handleSaveOrUpdate = (itemSelected) => {
        handleSaveDeactivate(itemSelected);
        props.handleUpdate(itemSelected);
    }

    const styles = useStyles();

    return (
        <form onSubmit={() => handleSaveOrUpdate(newItem)}>
            <div className={styles.modal}>
                <h3>Deactivate Item</h3>
                <br />
                <TextField
                    name="messageDisabled"
                    required
                    value={newItem.messageDisabled}
                    className={styles.inputMaterial}
                    label="MessageDisabled"
                    onChange={handleOnChange}
                />
                <br />
                <br />
                <div align="right">
                    <Button type="submit" color="primary">Save</Button>
                    <Button onClick={modalOpenCloseDisabled}>Cancel</Button>
                </div>
            </div>
        </form>
    )
})



