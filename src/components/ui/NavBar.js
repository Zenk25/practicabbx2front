import React from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useDispatch } from 'react-redux';
import PersonIcon from '@material-ui/icons/Person';
import clsx from 'clsx';
import StoreIcon from '@material-ui/icons/Store';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import { LOGOUT } from '../../types/types';



const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  title: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));


export const Navbar = () => {

  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();


  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  function checkRole() {
    const currentRoles = localStorage.getItem('role');
    if (currentRoles && currentRoles === "\"ROLE_ADMIN\"") {
      return (

        <NavLink
          activeClassName="active"
          className="nav-item nav-link"
          exact
          to="/user"
        ><SupervisorAccountIcon className="mr-2"></SupervisorAccountIcon>
            Users
        </NavLink>
      );
    }
  }


  const handleLogout = () => {
    localStorage.clear();
    dispatch({
      type: LOGOUT
    });
    history.replace('/login');
  }


  return (


    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>


          <Typography variant="h6" className={classes.title}>

          </Typography>

          <span>
            <PersonIcon />{localStorage.getItem('user')}
          </span>

          <Button onClick={handleLogout} color="inherit">Logout</Button>
        </Toolbar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >

          <div className={classes.drawerHeader}>
            <span className="mr-5">
              <PersonIcon />{localStorage.getItem('user')}
            </span>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </div>
          <Divider />
          <br />
          <br />
          <ul style={{ listStyle: "none" }}>
            <li>
              <NavLink
                activeClassName="active"
                className="nav-item nav-link"
                exact
                to="/item">
                  <StoreIcon className="mr-2"></StoreIcon>
                       Items
              </NavLink>
            
            </li>
            <li>
              {checkRole()}
            </li>


          </ul>
          <br />
          <br />
          <Divider />

        </Drawer>
      </AppBar>
    </div>
  );
}

