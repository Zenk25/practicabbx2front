import { Fab, Modal } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import AddIcon from "@material-ui/icons/Add";
import { UserTable } from './Tables/UserTable';
import { UserDataColumns } from '../../constant/TableColumns/user/UserDataColumns';
import { useDispatch, useSelector } from 'react-redux';
import { addUser, deleteUserServ, getUserByUsername, getUsers } from '../../services/UserService';
import { ModalDelete } from './Modals/ModalDelete';
import { ModalSave } from './Modals/ModalSave';
import { getRoles } from '../../services/RoleService';



export function UserComponent({ history }) {

    const dispatch = useDispatch();
    const [flagState, setFlagState] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [modalSave, setModalSave] = useState(false);


    useEffect(() => {
        getUsers(dispatch);
        getRoles(dispatch);
    }, [dispatch, flagState]);

    const modalOpenCloseDelete = () => {
        setModalDelete(!modalDelete);
    }

    const modalOpenCloseSave = () => {
        setModalSave(!modalSave);
    }

    const users = useSelector((state) => state.users.users);

    const createData = (id, username, role) => {
        return { id, username, role }
    }

    const data = users.map((user, id) => {
        return (
            createData(id, user.username, user.role)
        )
    }
    )

    const handleOpenCloseDelete = (username) => {
        getUserByUsername(dispatch, username);
        modalOpenCloseDelete();
    }

    const handleDeleteUser = (username) => {
        deleteUserServ(dispatch, username);
        setFlagState(true);
        modalOpenCloseDelete();
    }

    const handleSaveUser = (newUser) => {
        addUser(dispatch, newUser);
        setFlagState(true);
        modalOpenCloseSave();
    }

    const roles = useSelector((state) => state.roles.roles);

    if (users !== null && users !== undefined) {

        return (
            <>
                <div style={{ display: 'vertical-flex' }}>
                    <div style={{ marginBottom: 12 }} className="text-center">
                        <br />
                        <div style={{ height: 600, width: '100%' }} >
                            <UserTable
                                columns={() => UserDataColumns(handleOpenCloseDelete)}
                                data={data}
                            />
                        </div>
                    </div>
                    <div style={{ marginTop: 50 }}>
                        <Fab style={{ margin: 10 }} color="primary" aria-label="add" onClick={modalOpenCloseSave}>
                            <AddIcon />
                        </Fab>
                        <Modal open={modalDelete} onClose={modalOpenCloseDelete}>
                            <ModalDelete handleDeleteUser={handleDeleteUser} />
                        </Modal>
                        <Modal open={modalSave} onClose={modalOpenCloseSave}>
                            <ModalSave handleSaveUser={handleSaveUser} modalOpenCloseSave={modalOpenCloseSave} roles={roles}/>
                        </Modal>
                        {/*Boton para cambiar el filtro del Estado
                    <Fab style={{margin: 10}} onClick={() => handleStateChange()} color="secondary" aria-label="favorite">
                        <FavoriteIcon />
                    </Fab>
                    */}
                    </div>
                </div>
            </>
        );
    }
}