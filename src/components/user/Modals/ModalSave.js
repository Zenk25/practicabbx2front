import { Button, InputLabel, makeStyles, Select, TextField } from '@material-ui/core';
import React, { useState } from 'react';



export const ModalSave = React.forwardRef((props, ref) => {


    const useStyles = makeStyles((theme) => ({
        modal: {
            position: "absolute",
            width: 400,
            backgroundColor: theme.palette.background.paper,
            border: "2px solid #000",
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
        },

        margin: {
            margin: theme.spacing(1),
        },
        iconos: {
            cursor: "pointer",
        },
        inputMaterial: {
            width: "100%",
            margin: theme.spacing(1)
        },
        inputLabel: {
            width: "100%",
            marginTop: theme.spacing(1),
            textDecoration: "underline",
            color: theme.palette.text.primary

        }
    }));



    const [error, setError] = useState(true);

    const [newUser, setNewUser] = useState({
        username: "",
        password: "",
        role: "",
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setNewUser((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleConfirmPassword = (e) => {
        const { value } = e.target;
        if (value && newUser.password) {
            if (value !== newUser.password) {
                setError(true);
            }else{
                setError(false);
            }
        }
    }


    const handleChangeMultiple = (e) => {
        const { value } = e.target;
        setNewUser((prevState) => ({
            ...prevState,
            role: value
        }));

    };



    const styles = useStyles();

    return (
        <div className={styles.modal}>
            <h3>Add New User</h3>
            <br />
            <TextField
                required
                type="string"
                name="username"
                value={newUser.username}
                className={styles.inputMaterial}
                label="Username"
                onChange={handleChange}
            />
            <br />
            <TextField
                required
                type="password"
                name="password"
                label="Password"
                value={newUser.password}
                className={styles.inputMaterial}
                onChange={handleChange}

            />
            <br />
            <TextField
                type="password"
                name="confPassword"
                label="Confirm Password"
                required
                className={styles.inputMaterial}
                onChange={handleConfirmPassword}
                error={error}
            />
            <InputLabel className="mt-3" ><strong style={{marginLeft: 10}}>Role</strong></InputLabel>
            <Select
                    name="role"
                    className={styles.inputMaterial}
                    label="Role"
                    onChange={handleChangeMultiple}
                    >
                    {props.roles &&
                        props.roles.map((role) => (
                            <option
                                key={role.role}
                                value={role}
                            >
                                {role.description}
                            </option>
                        ))}
                </Select>
            <br />
            <br />
            <br />
            <div align="right">
                <Button onClick={() => props.handleSaveUser(newUser)}  disabled={error} color="primary">
                    Add
            </Button>
                <Button onClick={props.modalOpenCloseSave}>Exit</Button>
            </div>
        </div>

    )
})



