import { Button, makeStyles } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';



export const ModalDelete = React.forwardRef((props, ref) => {


    const useStyles = makeStyles((theme) => ({
        modal: {
            position: "absolute",
            width: 400,
            backgroundColor: theme.palette.background.paper,
            border: "2px solid #000",
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
        },

        margin: {
            margin: theme.spacing(1),
        },
        iconos: {
            cursor: "pointer",
        },
        inputMaterial: {
            width: "100%",
            margin: theme.spacing(1)
        },
        inputLabel: {
            width: "100%",
            marginTop: theme.spacing(1),
            textDecoration: "underline",
            color: theme.palette.text.primary

        }
    }));

    const username = useSelector(state => state.users.userSelected.username);


    

    const styles = useStyles();

    return (
        <div className={styles.modal}>
            <h3>Delete User</h3>
            <span>¿Do you want to delete this user?</span>
            <div align="right">
                <Button onClick={() => props.handleDeleteUser(username)} color="primary">
                    Delete
            </Button>
                <Button onClick={props.modalOpenCloseDelete}>Cancel</Button>
            </div>
        </div>

    )
})



